import 'package:sqflite/sqflite.dart';

import 'database_provider.dart';
import 'cat.dart';

class CatDao {
  static Future<void> insertCat(Cat cat) async {
    final db = await DatabaseProvider.database;
    await db.insert(
      'cats',
      cat.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  //getค่าออกมาดู
  static Future<List<Cat>> cats() async {
    final db = await DatabaseProvider.database;
    final List<Map<String, dynamic>> maps =
        await db.query('cats'); //ได้เป็น list ของ map
    return List.generate(maps.length, (i) {
      return Cat(
        id: maps[i]['id'],
        name: maps[i]['name'],
        age: maps[i]['age'],
      );
    });
  }

  //update
  static Future<void> updateCat(Cat cat) async {
    final db = await DatabaseProvider.database;
    await db.update(
      'cats',
      cat.toMap(),
      where: 'id = ?',
      whereArgs: [cat.id],
    );
  }

//delete
  static Future<void> deleteCat(int id) async {
    final db = await DatabaseProvider.database;
    await db.delete(
      'cats',
      where: 'id = ?',
      whereArgs: [id],
    );
  }
}
