import 'package:flutter/material.dart';
import './cat.dart';
import 'cat_dao.dart';

//sqlite ใช้บน chrome ไม่ได้ ต้องใช้โมบายรัน
void main() async {
  //test insert
  var meaw = Cat(
    id: 0,
    name: 'Fido',
    age: 2,
  );
  var miko = Cat(
    id: 1,
    name: 'Miko',
    age: 15,
  );
  await CatDao.insertCat(meaw);
  await CatDao.insertCat(miko);

  print(await CatDao.cats());
 

  
  meaw = Cat(
    id: meaw.id,
    name: 'Jomkean',
    age: meaw.age+2,
  );
  await CatDao.updateCat(meaw);
  print(await CatDao.cats());


  await CatDao.deleteCat(1);
  print(await CatDao.cats());
}


